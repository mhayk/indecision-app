console.log('Hi baby!')

const app = {
    title: 'Indecision App',
    subtitle: 'Put your life in the hands of a computer',
    options: [],
    visibility: false
}

function test(location) {
    if(location)
        return 'BRAZIL'
    else
        return 'UK'
}

const onFormSubmit = e => {
    e.preventDefault()

    const option = e.target.elements.option.value

    if (option) {
        app.options.push(option)
        e.target.elements.option.value = ''
    }
    render()
}

const removeall = () => {
    app.options = []
    render()
}

const onMakeDecision = () => {
    const randomNum = Math.floor(Math.random() * app.options.length)
    const option = app.options[randomNum]
    alert(option)
}

const onShowDetails = () => {
    app.visibility = !app.visibility
    render()
}

const render = () => {
    var template = (
        <div>
            <h1>{ app.title }</h1>
            { app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length > 0 ? 'Here are your options:' : 'No options'}</p>
            <button disabled={app.options.length === 0 } onClick={onMakeDecision}>What should I do ?</button>
            <button onClick={removeall}>Remove All</button>
            <ol>
                {app.options.map( n => <li key={n}>{n}</li>)}
            </ol>
            <form  onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button>Add Option</button>
            </form>
            <h1>Visibility Toggle</h1>
            <button onClick={onShowDetails}>{ app.visibility ? 'Hide details' : 'Show details'}</button>
            <p>{app.visibility ? 'Hey. These are some details yo can now see!' : '' }</p>
        </div>
    );

    var appRoot = document.getElementById('app')
    ReactDOM.render(template, appRoot)
}

render()