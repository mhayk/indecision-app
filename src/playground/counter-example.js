class Counter extends React.Component {
    constructor(props) {
        super(props)
        this.handlerAddOne = this.handlerAddOne.bind(this)
        this.handlerMinusOne = this.handlerMinusOne.bind(this)
        this.handerReset = this.handerReset.bind(this)
        this.state = {
            count: 0
        }
    }
    componentDidMount() {
        try {
            const json = localStorage.getItem('count')
            const count = JSON.parse(json)

            if(count)
                this.setState( () => ({count}))
        } catch (error) {
            
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.count !== this.state.count) {
            const json = JSON.stringify(this.state.count)
            localStorage.setItem("count", json)
        }

    }
    handlerAddOne() {
        this.setState( prevState => {
            return {
                count: prevState.count + 1
            }
        })
    }
    handlerMinusOne() {
        this.setState( prevState => {
            return {
                count: prevState.count - 1
            }
        })
    }
    handerReset() {
        this.setState({count: 0})
    }
    render() {
        return (
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.handlerAddOne}>+1</button>
                <button onClick={this.handlerMinusOne}>-1</button>
                <button onClick={this.handerReset}>reset</button>
            </div>
        )
    }
}

ReactDOM.render(<Counter />, document.getElementById('app'))