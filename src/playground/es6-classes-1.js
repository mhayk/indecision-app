class Person {
    constructor(name, age = 0) {
        this.name = name
        this.age = age
    }

    getGretting() {
        return `Hi. I am ${this.name}!`
    }

    getDescription() {
        return `${this.name} is ${this.age} year(s) old.`
    }
}

class Student extends Person {
    constructor(name, age, major) {
        super(name,age)
        this.major = major
    }

    hasMajor() {
        return !!this.major
    }

    getDescription() {
        let description = super.getDescription()
        if (this.hasMajor()) {
            description += ` Their major is ${this.major}`
        }
        return description
    }
}

class Traveler extends Person {
    constructor(name, age, homeLocation) {
        super(name,age)
        this.homeLocation = homeLocation
    }

    getGretting() {
        let gretting = super.getGretting()
        if(this.hasHomeLocation())
            return gretting += ` I'm visiting from ${this.homeLocation}`
        else
            return gretting
    }

    hasHomeLocation() {
        return this.homeLocation
    }
}

const me = new Student('Mhayk', 31, 'Computer Science')
console.log(me.getGretting())
console.log(me.getDescription())
console.log(me.hasMajor())

const alana = new Student('Alana', 29)
console.log(alana.getDescription())
console.log(alana.hasMajor())

const mhayk = new Traveler('Mhayk', 31, 'Brazil')
console.log(mhayk.getGretting())